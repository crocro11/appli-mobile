function initialise_video() {
    $("#playpause").on("click", togglePlay);
    $("#stop").on("click", stopVideo);
    $("#rewind").on("click", downPlaybackSpeed);
    $("#ffwd").on("click", upPlaybackSpeed);
    $("#volumeDown").on("click", downVolume);
    $("#volumeUp").on("click", upVolume);
    $("#mute").on("click", toggleMute);

    $("#rewind").class;
    $("#ffwd").css("background-position", "15px 0px");
    $("#playpause").css("background-position", "30px 0px");
    $("#stop").css("background-position", "60px 0px");
    $("#volumeDown").css("background-position", "90px 0px");
    $("#volumeUp").css("background-position", "105px 100px");
    $("#mute").css("background-position", "120px 100px");
  // Remove the default controls from the video (if JavaScript is disabled, it will still display)
    var video = $("video")[0];
    video.controls = false;
    // Get a handle to the play/pause button elements
    var ppbutton = $("#playpause");
    // Add the playbutton on start
    // Add events for play, pause and ended, in case the video is controlled via something else
    // (e.g. right clicking on the video in Firefox gives you a set of controls)
    video.addEventListener('play', function() {
    }, false);
    video.addEventListener('pause', function() {
    }, false);
    video.addEventListener('ended', function() {
        this.pause();
    }, false);
    // Add an event for when the volumechange, to update the current volume display
    video.addEventListener('volumechange', updateVolumeDisplay, false);
    // Add an event to update the file duration display when the value has been retrieved
    video.addEventListener('durationchange', updateDurationDisplay, false);
    // Add an event to update the current video time display when the time updates
    video.addEventListener('timeupdate', updateCurrentTimeDisplay, false);
    // Add an event to update the current video playback rate when it changes (if supported)
    video.addEventListener('ratechange', updatePlaybackRateDisplay, false);
    // Add an event to update the amount of the video that has loaded
    video.addEventListener('progress', updatePercentLoadedDisplay, false);
    // Set some display values
    setDisplay();

    // This function toggles the play and pause button depending on the current status of the video
    // It is called when the play or pause buttons are clicked
    

    // This function toggles the play and pause button depending on the current status of the video
    // It is called when the play or pause buttons are clicked
    function togglePlay() {
        var video = $("video")[0];
        var ppbutton = $("#ppbutton");
        // Check if the video is paused or has ended
        if (video.paused || video.ended) {
            // If it's ended, then set the current time to 0
            if (video.ended)
                video.currentTime = 0;
            // Play the video
            video.play();
        } else {
            // Change the button text to "play"
            // Pause the video
            video.pause();
        }
    }

    // This function stops the video from playing and resets it to the start
    function stopVideo() {
        var video = $("video")[0];
        // Pause the video
        video.pause();
        // Set the current time back to the start of the video (0)
        video.currentTime = 0;
    }

    // This function toggles the mute value and changes the mute button text accordingly
    // Note: There is no event raised when a video is muted
    function toggleMute() {
        var video = $("video")[0];
        var mute = $("#mute");
        if (video.muted) {
            video.muted = false;
        } else {
            video.muted = true;
        }
        // As no event is raised, the display needs to be updated here
        updateMutedDisplay();
    }

    // This function will change the volume of the video up or down
    // It is called when either the volumeDown or volumeUp buttons are clicked
    function downVolume(direction) {
        var video = $("video")[0];
        // Round the current video volume to the nearest decimal place
        var volume = Math.floor(video.volume * 10) / 10;
        // Unmute the video (in case it's muted)
        video.muted = false;
        // If the current volume is less than or equal to 0.1, set the volume to be 0
        if (volume <= 0.1)
            video.volume = 0;
        // Otherwise reduce the volume by 0.1
        else
            video.volume -= 0.1;
    }

    // This function will change the volume of the video up or down
    // It is called when either the volumeDown or volumeUp buttons are clicked
    function upVolume(direction) {
        var video = $("video")[0];
        // Round the current video volume to the nearest decimal place
        var volume = Math.floor(video.volume * 10) / 10;
        // Unmute the video (in case it's muted)
        video.muted = false;
        // If the volume is to be reduced
        // If the current volume is greater than or equal to 0.9, then set it to be 1
        if (volume >= 0.9)
            video.volume = 1;
        // Otherwise increase the volume by 0.1
        else
            video.volume += 0.1;
    }

    // This function changes the playback speed
    function downPlaybackSpeed(direction) {
        var video = $("video")[0];
        // Check if playbackRate is actually supported (Webkit browsers only)
        // Note: Safari will actually play the video backwards if the playbackRate is negative
        //       Chrome won't play at all when the playbackRate is negative
        if (video.playbackRate != undefined) {
            video.playbackRate -= 1;
        }
        // If playbackRate is not supported, then set the current time back or forward by 1
        else {
            video.currentTime -= 1;
        }
    }

    // This function changes the playback speed
    function upPlaybackSpeed(direction) {
        var video = $("video")[0];
        // Check if playbackRate is actually supported (Webkit browsers only)
        // Note: Safari will actually play the video backwards if the playbackRate is negative
        //       Chrome won't play at all when the playbackRate is negative
        if (video.playbackRate != undefined) {
            video.playbackRate += 1;
        }
        // If playbackRate is not supported, then set the current time back or forward by 1
        else {
            video.currentTime += 1;
        }
    }

    // This function initialises the various video information displays on screen
    function setDisplay() {
        var video = $("video")[0];
        // Set the file duration
        updateDurationDisplay();
        // Set the current time
        updateCurrentTimeDisplay();
        // Set the volume display
        updateVolumeDisplay();
        // Set the muted display
        updateMutedDisplay();
        // Set the playback rate display
        updatePlaybackRateDisplay();
    }

    // This function simply displays the current video time and the percentage of the video played
    function updateCurrentTimeDisplay() {
        var video = $("video")[0];
        $("#currentTime").text(video.currentTime);
        // Calculate and set the progress bar of the video played
        var value = 0;
        if (video.currentTime > 0)
            value = Math.floor((100 / video.duration) * video.currentTime);
        /*if (video.currentTime == 0) value = 0;
         else value = Math.floor((100 / video.duration) * video.currentTime);*/
        var played = $("#played")[0];
        played.style.width = value + "%";
        played.title = value + "%";
    }

    // This function displays the file duration
    function updateDurationDisplay() {
        var video = $("video")[0];
        $("#fileDuration").text(video.duration);
    }

    // This function simply displays the current volume of the video
    function updateVolumeDisplay() {
        var video = $("video")[0];
        $("#currentVolume").text(Math.floor(video.volume * 10));
    }

    // This function displays the video's muted value
    function updateMutedDisplay() {
        var video = $("video")[0];
        $("#muteValue").text(video.muted);
    }

    // This function simply displays the current video time
    function updatePlaybackRateDisplay() {
        var video = $("video")[0];
        if (video.playbackRate != undefined)
            $("#playbackRate").text(video.playbackRate);
        else
            $("#playbackRate").text("Not supported");
    }

    // This function displays the percentage of how much the video is seekable
    // Note: this doesn't work n Firefox or Opera and Safari, Chrome and IE9 always show the full duration
    function updatePercentLoadedDisplay() {
        var video = $("video")[0];
        var value;
        if (video.seekable == undefined)
            value = "Unknown";
        // Calculate the percentage of the video that's seekable as a percentage
        else
            value = 100 * (video.duration / video.seekable.end(0)) + "%";
        $("#percentLoaded").text(value);
    }
}